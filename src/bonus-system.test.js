import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");

describe('Bonus tests', () => {
    let app;
    console.log("Tests started");
    test('Valid standard test',  (done) => {
        let a =200;
        expect(calculateBonuses('Standard', a)).toEqual(0.05);
        done();
    });

    test('Valid standard test1',  (done) => {
        let a = 10000;
        expect(calculateBonuses('Standard', a)).toEqual(0.05*1.5);
        done();
    });

    test('Valid standard test2',  (done) => {
        let a =50000;
        expect(calculateBonuses('Standard', a)).toEqual(0.05*2);
        done();
    });

    test('Valid  big standard test',  (done) => {
        let a = 100000;
        expect(calculateBonuses('Standard', a)).toEqual(0.05*2.5);
        done();
    });


    test('Valid Premium test',  (done) => {
        let a = 200;
        expect(calculateBonuses('Premium', a)).toEqual(0.1);
        done();
    });

    test('Valid Premium test1',  (done) => {
        let a =10000;
        expect(calculateBonuses('Premium', a)).toEqual(0.1*1.5);
        done();
    });


    test('Valid Premium test2',  (done) => {
        let a = 50000;
        expect(calculateBonuses('Premium', a)).toEqual(0.1*2);
        done();
    });


    test('Valid Premium Big Amount test',  (done) => {
        let a = 100000;
        expect(calculateBonuses('Premium', a)).toEqual(0.1*2.5);
        done();
    });



    test('Valid Diamond test1',  (done) => {
        let a = 200;
        expect(calculateBonuses('Diamond', a)).toEqual(0.2);
        done();
    });


    test('Valid Diamond test2',  (done) => {
        let a = 10000;
        expect(calculateBonuses('Diamond', a)).toEqual(0.2*1.5);
        done();
    });


    test('Valid Diamond test',  (done) => {
        let a = 50000;
        expect(calculateBonuses('Diamond', a)).toEqual(0.2*2);
        done();
    });

    test('Valid Diamond test3',  (done) => {
        let a = 100000;
        expect(calculateBonuses('Diamond', a)).toEqual(0.2*2.5);
        done();
    });

    test('Valid Nonsense1 test',  (done) => {
        let a = 100;
        expect(calculateBonuses('aaaa', a)).toEqual(0);
        done();
    });

    test('Valid Nonsense2 test',  (done) => {
        let a = 10000;
        expect(calculateBonuses('aaa', a)).toEqual(0);
        done();
    });

    test('Valid Nonsense3 test',  (done) => {
        let a = 50000;
        expect(calculateBonuses('aaa', a)).toEqual(0);
        done();
    });

    test('Valid Nonsense test',  (done) => {
        let a = 1000000;
        expect(calculateBonuses('aaa', a)).toEqual(0);
        done();
    });

   test('Valid Absolute Nonsense test',  (done) => {
        let a ='';
        expect(calculateBonuses('fss', a)).toEqual(0);
        done();
    });


    console.log('Tests Finished');

});
